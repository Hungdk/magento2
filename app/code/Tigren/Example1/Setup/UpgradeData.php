<?php

namespace Tigren\Example1\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    private $eavConfig;

    private $attributeResource;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Customer\Model\ResourceModel\Attribute $attributeResource
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeResource = $attributeResource;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup->startSetup();

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.1') < 0) {

            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                'is_vip',
                [
                    'type' => 'int',
                    'label' => 'Is Vip',
                    'input' => 'boolean',
                    'required' => false,
                    'sort_order' => 4,
                    'default' => 0,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'group' => 'General Information',
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('quote'),
                'is_vip',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'size' => 1,
                    'nullable' => true,
                    'comment' => 'Is Vip'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('sales_order'),
                'is_vip',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'size' => 1,
                    'nullable' => true,
                    'comment' => 'Is Vip'
                ]
            );
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.2') < 0) {
            $attribute = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'is_vip');
            $attribute->setData('used_in_forms', ['adminhtml_customer']);
            $this->attributeResource->save($attribute);
        }



        $installer->endSetup();
    }
}