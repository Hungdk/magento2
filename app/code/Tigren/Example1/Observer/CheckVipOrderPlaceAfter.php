<?php

namespace Tigren\Example1\Observer;

use Magento\Framework\Event\Observer;

/**
 * Class CheckVipOrderPlaceAfter
 * @package Tigren\Example1\Observer
 */
class CheckVipOrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_modelCustomer;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $_customerFactory;

    /**
     * CheckVipOrderPlaceAfter constructor.
     * @param \Magento\Customer\Model\Customer $modelCustomer
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory
     */
    public function __construct(
        \Magento\Customer\Model\Customer $modelCustomer,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        $this->_modelCustomer = $modelCustomer;
        $this->_customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();
        if ($this->checkVipQuote($quote)) {
            $observer->getOrder()->setIsVip(1);
            $customerId = $observer->getOrder()->getCustomerId();
            $customer = $this->_customerFactory->create()->load($customerId);
            $customer->setData('is_vip', 1)
                ->setAttributeSetId(\Magento\Customer\Api\CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER);
            $customer->save();
        } else $observer->getOrder()->setIsVip(0);
    }

    /**
     * @param $quote
     * @return int
     */
    public function checkVipQuote($quote)
    {
        if ($quote->getIsVip()) return 1;
        else return 0;
    }
}