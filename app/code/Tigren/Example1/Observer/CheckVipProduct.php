<?php

namespace Tigren\Example1\Observer;


/**
 * Class CheckVipProduct
 * @package Tigren\Example1\Observer
 */
class CheckVipProduct implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_productCollection;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * CheckVipProduct constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory
    )
    {
        $this->_productCollection = $productCollection;
        $this->_productFactory = $productFactory;
        $this->_quoteFactory = $quoteFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getCart()->getQuote();
        $items = $quote->getAllItems();
        if ($this->isVip($items)) {
            $observer->getCart()->getQuote()->setIsVip(1);
        } else {
            $observer->getCart()->getQuote()->setIsVip(0);
        }

        return;
    }

    /**
     * @param $items
     * @return int
     */
    protected function isVip($items)
    {
        foreach ($items as $item) {
            $product = $item->getProduct();
            if ($product->getData('is_vip')) {
                return 1;
            }
        }
        return 0;
    }

}