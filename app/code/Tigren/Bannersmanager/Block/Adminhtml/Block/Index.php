<?php
namespace Tigren\Bannersmanager\Block\Adminhtml\Block;

use Magento\Backend\Block\Widget\Grid\Container;
class Index extends Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_block';
        $this->_blockGroup = 'Tigren Block';
        $this->_headerText = __('Block');
        $this->_addButtonLabel = __('Add New Block');
        parent::_construct();
    }
}