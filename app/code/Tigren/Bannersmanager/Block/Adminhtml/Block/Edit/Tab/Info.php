<?php

namespace Tigren\Bannersmanager\Block\Adminhtml\Block\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Tigren\Bannersmanager\Model\Source\CustomerGroup;


class Info extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    /**
     * @var \Tigren\Bannersmanager\Model\Source\CustomerGroup
     */
    protected $_customerGroup;


//    /**
//     * @var \Tutorial\SimpleNews\Model\Config\Status
//     */
//    protected $_newsStatus;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param CustomerGroup $customerGroup


     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        CustomerGroup $customerGroup,


        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_customerGroup = $customerGroup;


        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        /** @var \Tigren\Bannersmanager\Model\Block $model */
        $model = $this->_coreRegistry->registry('bannersmanager_block');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('block_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information')]
        );

        if ($model->getId()) {
            $fieldset->addField('block_id', 'hidden', ['name' => 'block_id']);
        }

        // Title Field
        $fieldset->addField(
            'block_title',
            'text',
            ['name' => 'block_title', 'label' => __('Block Title'), 'title' => __('Block Title'), 'required' => true]
        );

        // Customer Group Field
        $customerGroup = $this->_customerGroup->toOptionArray();
        $arrCustomerGroupID = explode('|', $model->getCustomerGroupId());
        $model->setData('customer_group_id', $arrCustomerGroupID);

        $fieldset->addField(
            'customer_group_id',
            'multiselect',
            [
                'name' => 'customer_group[]',
                'label' => __('Customer Group'),
                'title' => __('Customer Group'),
                'required' => true,
                'values' => $customerGroup,
                'disabled' => false
            ]
        );

        // Block Position Field
        $fieldset->addField(
           'position',
           'select',
            [
                'name' => 'position',
                'label' => __('Block Position'),
                'title' => __('Block Position'),
                'values' => [
                    [
                        'label' => __('------- Please choose position -------'),
                        'value' => '',
                    ],
                    [
                        'label' => __('Popular positions'),
                        'value' => [
                            ['value' => 'cms-page-content-top', 'label' => __('Homepage-Content-Top')],
                        ],
                    ],
                    [
                        'label' => __('Default for using in CMS page template'),
                        'value' => [
                            ['value' => 'custom', 'label' => __('Custom')],
                        ],
                    ],
                    [
                        'label' => __('General (will be disaplyed on all pages)'),
                        'value' => [
                            ['value' => 'sidebar-right-top', 'label' => __('Sidebar-Top-Right')],
                            ['value' => 'sidebar-right-bottom', 'label' => __('Sidebar-Bottom-Right')],
                            ['value' => 'sidebar-left-top', 'label' => __('Sidebar-Top-Left')],
                            ['value' => 'sidebar-left-bottom', 'label' => __('Sidebar-Bottom-Left')],
                            ['value' => 'content-top', 'label' => __('Content-Top')],
                            ['value' => 'menu-top', 'label' => __('Menu-Top')],
                            ['value' => 'menu-bottom', 'label' => __('Menu-Bottom')],
                            ['value' => 'page-bottom', 'label' => __('Page-Bottom')],
                        ],
                    ],
                    [
                        'label' => __('Catalog and product'),
                        'value' => [
                            ['value' => 'catalog-sidebar-right-top', 'label' => __('Catalog-Sidebar-Top-Right')],
                            ['value' => 'catalog-sidebar-right-bottom', 'label' => __('Catalog-Sidebar-Bottom-Right')],
                            ['value' => 'catalog-sidebar-left-top', 'label' => __('Catalog-Sidebar-Top-Left')],
                            ['value' => 'catalog-sidebar-left-bottom', 'label' => __('Catalog-Sidebar-Bottom-Left')],
                            ['value' => 'catalog-content-top', 'label' => __('Catalog-Content-Top')],
                            ['value' => 'catalog-menu-top', 'label' => __('Catalog-Menu-Top')],
                            ['value' => 'catalog-menu-bottom', 'label' => __('Catalog-Menu-Bottom')],
                            ['value' => 'catalog-page-bottom', 'label' => __('Catalog-Page-Bottom')],
                        ],
                    ],
                    [
                        'label' => __('Category only'),
                        'value' => [
                            ['value' => 'category-sidebar-right-top', 'label' => __('Category-Sidebar-Top-Right')],
                            ['value' => 'category-sidebar-right-bottom', 'label' => __('Category-Sidebar-Bottom-Right')],
                            ['value' => 'category-sidebar-left-top', 'label' => __('Category-Sidebar-Top-Left')],
                            ['value' => 'category-sidebar-left-bottom', 'label' => __('Category-Sidebar-Bottom-Left')],
                            ['value' => 'category-content-top', 'label' => __('Category-Content-Top')],
                            ['value' => 'category-menu-top', 'label' => __('Category-Menu-Top')],
                            ['value' => 'category-menu-bottom', 'label' => __('Category-Menu-Bottom')],
                            ['value' => 'category-page-bottom', 'label' => __('Category-Page-Bottom')],
                        ],
                    ],
                    [
                        'label' => __('Product only'),
                        'value' => [
                            ['value' => 'product-sidebar-right-top', 'label' => __('Product-Sidebar-Top-Right')],
                            ['value' => 'product-sidebar-right-bottom', 'label' => __('Product-Sidebar-Bottom-Right')],
                            ['value' => 'product-sidebar-left-top', 'label' => __('Product-Sidebar-Top-Left')],
                            ['value' => 'product-content-top', 'label' => __('Product-Content-Top')],
                            ['value' => 'product-menu-top', 'label' => __('Product-Menu-Top')],
                            ['value' => 'product-menu-bottom', 'label' => __('Product-Menu-Bottom')],
                            ['value' => 'product-page-bottom', 'label' => __('Product-Page-Bottom')],
                        ],
                    ],
                    [
                        'label' => __('Customer'),
                        'value' => [
                            ['value' => 'customer-content-top', 'label' => __('Customer-Content-Top')],
                            ['value' => 'customer-sidebar-main-top', 'label' => __('Customer-Siderbar-Main-Top')],
                            ['value' => 'customer-sidebar-main-bottom', 'label' => __('Customer-Siderbar-Main-Bottom')],
                        ],
                    ],
                    [
                        'label' => __('Cart & Checkout'),
                        'value' => [
                            ['value' => 'cart-content-top', 'label' => __('Cart-Content-Top')],
                            ['value' => 'checkout-content-top', 'label' => __('Checkout-Content-Top')],
                        ],
                    ],
                ],
                'required' => true]

        );

        // Category Type
        $fieldset->addField(
            'category',
            'select',
            [
                'name' => 'category',
                'label' => __('Category Type'),
                'title' => __('Category Type'),
                'values' => [
                    [
                        'label' => __('All Categories'),
                        'value' => '1',
                    ],
                    [
                        'label' => __('All Categories except below'),
                        'value' => '2',
                    ],
                    [
                        'label' => __('Certain Categories'),
                        'value' => '3',
                    ]
                ],
                'required' => true
            ]
        );

        // From Field
        $fieldset->addField(
            'from',
            'date',
            [
                'name' => 'from',
                'label' => __('From'),
                'title' => __('From'),
                'date_format' => 'yyyy-MM-dd',
                'time_format' => 'hh:mm:ss',
                'required' => false]
        );

        // To Field
        $fieldset->addField(
            'to',
            'date',
            [
                'name' => 'to',
                'label' => __('To'),
                'title' => __('To'),
                'date_format' => 'yyyy-MM-dd',
                'time_format' => 'hh:mm:ss',
                'required' => false]
        );

        // Display Type
        $fieldset->addField(
            'display_type',
            'select',
            [
                'name' => 'display_type',
                'label' => __('Display Type'),
                'title' => __('Display Type'),
                'values' => [
                    [
                        'label' => __('-- Select Type --'),
                        'value' => '0',
                    ],
                    [
                        'label' => __('All Images'),
                        'value' => '1',
                    ],
                    [
                        'label' => __('Random'),
                        'value' => '2',
                    ],
                    [
                        'label' => __('Slider'),
                        'value' => '3',
                    ],
                    [
                        'label' => __('Slider with Description'),
                        'value' => '4',
                    ],
                    [
                        'label' => __('Basic Slider with Custom Direction Navigation'),
                        'value' => '5',
                    ],
                    [
                        'label' => __('Slider with Min and Max Ranges'),
                        'value' => '6',
                    ],
                    [
                        'label' => __('Basic Carousel'),
                        'value' => '7',
                    ],
                    [
                        'label' => __('Fade'),
                        'value' => '8',
                    ],
                    [
                        'label' => __('Fade with Description'),
                        'value' => '9',
                    ],
                ],
                'required' => true
            ]
        );

        // Min Image Field
        $fieldset->addField(
            'min',
            'text',
            [
                'name' => 'min',
                'label' => __('Min Images'),
                'title' => __('Min Images'),
                'class' => 'validate-number',
                'required' => false]
        );

        // Max Image Field
        $fieldset->addField(
            'max',
            'text',
            [
                'name' => 'max',
                'label' => __('Max Images'),
                'title' => __('Max Images'),
                'class' => 'validate-number',
                'required' => false]
        );

        // Status Field
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Is Active'),
                'title' => __('Is Active'),
                'values' => [
                    ['label' => __('No'), 'value' => 0],
                    ['label' => __('Yes'), 'value' => 1],
                ],
                'required' => true
            ]
        );

        // Sort Order Field
        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => __('Sort Order'),
                'title' => __('Sort Order'),
                'class' => 'validate-number',
                'required' => false]
        );


        $form->setValues($model->getData());

        $this->setForm($form);



        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Block Info');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Block Info');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}