<?php

namespace Tigren\Bannersmanager\Block\Adminhtml\Block\Edit\Tab;

use Tigren\Bannersmanager\Model\Banner;

class BannerGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Tigren\Bannersmanager\Model\Banner
     */
    protected $_banner;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param Banner $banner
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        Banner $banner,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_banner = $banner;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('tigren_bannergrid');
        $this->setDefaultSort('banner_id');
        $this->setUseAjax(true);
    }


    /**
     * @return BannerGrid
     */
    protected function _prepareCollection()
    {
        $collection = $this->_banner->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $model = $this->_coreRegistry->registry('bannersmanager_block');
        $selectedBanners = $model->getData('banners');

        $this->addColumn(
            'selected_banners',
            [
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction',
                'type' => 'checkbox',
                'align'     => 'center',
                'field_name'=> 'selected_banners[]',
                'values' => $selectedBanners,
                'index' => 'banner_id',
            ]
        );
        $this->addColumn(
            'banner_id',
            [
                'header' => __('Banner Id'),
                'sortable' => true,
                'index' => 'banner_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'banner_title',
            [
                'header' => __('Title'),
                'index' => 'banner_title'
            ]
        );

        return parent::_prepareColumns();
    }

}