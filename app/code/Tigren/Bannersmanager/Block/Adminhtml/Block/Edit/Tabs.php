<?php

namespace Tigren\Bannersmanager\Block\Adminhtml\Block\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('block_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Block'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'block_info',
            [
                'label' => __('Block Information'),
                'title' => __('Block Information'),
                'content' => $this->getLayout()->createBlock(
                    'Tigren\Bannersmanager\Block\Adminhtml\Block\Edit\Tab\Info'
                )->toHtml(),
                'active' => true
            ]
        )
            ->addTab(
            'block_banner',
            [
                'label' => __('Select Banner'),
                'title' => __('Select Banner'),
                'content' => $this->getLayout()->createBlock(
                    'Tigren\Bannersmanager\Block\Adminhtml\Block\Edit\Tab\BannerGrid'
                )->toHtml(),
            ]
        );

        return parent::_beforeToHtml();
    }
}