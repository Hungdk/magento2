<?php
namespace Tigren\Bannersmanager\Block\Adminhtml\Banner;

use Magento\Backend\Block\Widget\Grid\Container;
class Index extends Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_banner';
        $this->_blockGroup = 'Tigren Banner';
        $this->_headerText = __('Banner');
        $this->_addButtonLabel = __('Add New Banner');
        parent::_construct();
    }
}