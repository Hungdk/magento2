<?php

namespace Tigren\Bannersmanager\Block\Adminhtml\Banner\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('banner_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Banner'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'banner_info',
            [
                'label' => __('Banner Information'),
                'title' => __('Banner Information'),
                'content' => $this->getLayout()->createBlock(
                    'Tigren\Bannersmanager\Block\Adminhtml\Banner\Edit\Tab\Info'
                )->toHtml(),
                'active' => true
            ]
        )
            ->addTab(
            'block_banner',
            [
                'label' => __('Select Block'),
                'title' => __('Select Block'),
                'content' => $this->getLayout()->createBlock(
                    'Tigren\Bannersmanager\Block\Adminhtml\Banner\Edit\Tab\BlockGrid'
                )->toHtml(),
            ]
        );

        return parent::_beforeToHtml();
    }
}