<?php

namespace Tigren\Bannersmanager\Block\Adminhtml\Banner\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Tigren\Bannersmanager\Model\Source\CustomerGroup;


class Info extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    /**
     * @var \Tigren\Bannersmanager\Model\Source\CustomerGroup
     */
    protected $_customerGroup;


//    /**
//     * @var \Tutorial\SimpleNews\Model\Config\Status
//     */
//    protected $_newsStatus;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param CustomerGroup $customerGroup


     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        CustomerGroup $customerGroup,


        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_customerGroup = $customerGroup;


        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        /** @var \Tigren\Bannersmanager\Model\Block $model */
        $model = $this->_coreRegistry->registry('bannersmanager_banner');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('banner_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information')]
        );

        if ($model->getId()) {
            $fieldset->addField('banner_id', 'hidden', ['name' => 'banner_id']);
        }

        // Title Field
        $fieldset->addField(
            'banner_title',
            'text',
            ['name' => 'banner_title', 'label' => __('Banner Title'), 'title' => __('Banner Title'), 'required' => true]
        );

        // Description Field
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => false,
            ]
        );

        // Image Field
        $fieldset->addField(
            'image',
            'image',
            [
                'title' => __('Image'),
                'label' => __('Image'),
                'name' => 'image',
            ]
        );

        // Target Field
        $fieldset->addField(
            'target',
            'select',
            [
                'name' => 'target',
                'label' => __('Target'),
                'title' => __('Target'),
                'values' => [
                    [
                        'label' => __('Open in the same window'),
                        'value' => '0',
                    ],
                    [
                        'label' => __('Open in new window'),
                        'value' => '1',
                    ]
                ],
                'required' => true
            ]
        );

        // From Field
        $fieldset->addField(
            'from',
            'date',
            [
                'name' => 'from',
                'label' => __('From'),
                'title' => __('From'),
                'date_format' => 'yyyy-MM-dd',
                'time_format' => 'hh:mm:ss',
                'required' => false]
        );

        // To Field
        $fieldset->addField(
            'to',
            'date',
            [
                'name' => 'to',
                'label' => __('To'),
                'title' => __('To'),
                'date_format' => 'yyyy-MM-dd',
                'time_format' => 'hh:mm:ss',
                'required' => false]
        );


        // Status Field
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Is Active'),
                'title' => __('Is Active'),
                'values' => [
                    ['label' => __('No'), 'value' => 0],
                    ['label' => __('Yes'), 'value' => 1],
                ],
                'required' => true
            ]
        );

        // Sort Order Field
        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => __('Sort Order'),
                'title' => __('Sort Order'),
                'class' => 'validate-number',
                'required' => false]
        );


        $form->setValues($model->getData());
        $this->setForm($form);



        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Banner Info');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Banner Info');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}