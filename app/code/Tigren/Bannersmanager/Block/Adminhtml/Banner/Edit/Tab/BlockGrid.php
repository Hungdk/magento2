<?php

namespace Tigren\Bannersmanager\Block\Adminhtml\Banner\Edit\Tab;

use Tigren\Bannersmanager\Model\Block;

class BlockGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Tigren\Bannersmanager\Model\Block
     */
    protected $_block;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param Block $block
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        Block $block,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_block = $block;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('tigren_blockgrid');
        $this->setDefaultSort('block_id');
        $this->setUseAjax(true);
    }


    /**
     * @return BlockGrid
     */
    protected function _prepareCollection()
    {
        $collection = $this->_block->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $model = $this->_coreRegistry->registry('bannersmanager_banner');
        $selectedBlocks = $model->getData('blocks');

        $this->addColumn(
            'selected_blocks',
            [
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction',
                'type' => 'checkbox',
                'align'     => 'center',
                'field_name'=> 'selected_blocks[]',
                'values' => $selectedBlocks,
                'index' => 'block_id',
            ]
        );
        $this->addColumn(
            'block_id',
            [
                'header' => __('Block Id'),
                'sortable' => true,
                'index' => 'block_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'block_title',
            [
                'header' => __('Title'),
                'index' => 'block_title'
            ]
        );

        return parent::_prepareColumns();
    }

}