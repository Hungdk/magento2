<?php

namespace Tigren\Bannersmanager\Block;
/**
 * Class Bannerslider
 * @package Tigren\Bannersmanager\Block
 */
class Bannerslider extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Tigren\Bannersmanager\Model\Block
     */
    protected $_block;

    /**
     * @var \Tigren\Bannersmanager\Model\Banner
     */
    protected $_banner;

    /**
     * @var null
     */
    protected $_blockIdCollection = null;

    /**
     * @var null
     */
    protected $_bannerIdCollection = null;

    /**
     * @var \Tigren\Bannersmanager\Model\ResourceModel\Block\CollectionFactory
     */
    protected $_blockCollectionFactory;

    /**
     * @var \Tigren\Bannersmanager\Model\ResourceModel\Banner\CollectionFactory
     */
    protected $_bannerCollectionFactory;
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Tigren\Bannersmanager\Model\Block $block
     * @param \Tigren\Bannersmanager\Model\Banner $banner
     * @param \Tigren\Bannersmanager\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory
     * @param \Tigren\Bannersmanager\Model\ResourceModel\Banner\CollectionFactory $bannerCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Tigren\Bannersmanager\Model\Block $block,
        \Tigren\Bannersmanager\Model\Banner $banner,
        \Tigren\Bannersmanager\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory,
        \Tigren\Bannersmanager\Model\ResourceModel\Banner\CollectionFactory $bannerCollectionFactory,
        array $data = []
    )
    {
        $this->_resource = $resource;
        $this->_block = $block;
        $this->_banner = $banner;
        $this->_blockCollectionFactory = $blockCollectionFactory;
        $this->_bannerCollectionFactory = $bannerCollectionFactory;
        parent::__construct(
            $context,
            $data
        );
    }


    /**
     * @return array
     */
    public function getAllBanner()
    {
        $enableBlockIds = $this->_blockCollectionFactory->create()->getEnableBlockIds();
        foreach ($enableBlockIds as $block_id) {
            $bannerCollection = $this->_bannerCollectionFactory->create();
            $bannersByBlockId = $bannerCollection->getBannersByBlockId($block_id);
            foreach ($bannersByBlockId as $banner) {
                $ban = $banner->getData();
                $banners[] = $ban;
            }
        }
        return $banners;
    }

    /**
     * @return array
     */
    public function getAllAvailableBanners()
    {
        $enableBlockIds = $this->_blockCollectionFactory->create()->getEnableBlockIds();
        foreach ($enableBlockIds as $block_id) {
            $bannerCollection = $this->_bannerCollectionFactory->create();
            $bannersByBlockId = $bannerCollection->getAvailBannersByBlockId($block_id);
            foreach ($bannersByBlockId as $banner) {
                $ban = $banner->getData();
                $banners[] = $ban;
            }
        }
        return $banners;
    }

    /**
     * @param $image
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaPath($image)
    {
        $path = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $path . $image;
    }



}