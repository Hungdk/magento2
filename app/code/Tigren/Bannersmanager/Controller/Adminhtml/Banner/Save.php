<?php

namespace Tigren\Bannersmanager\Controller\Adminhtml\Banner;

use Magento\Backend\App\Action;
use Tigren\Bannersmanager\Model\Banner;

class Save extends Action
{
    /**
     * @var \Tigren\Bannersmanager\Model\Banner
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \Tigren\Bannersmanager\Model\Banner $model
     */
    public function __construct(
        Action\Context $context,
        Banner $model
    )
    {
        parent::__construct($context);
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_Bannersmanager::banner_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Tigren\Bannersmanager\Model\Banner $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('banner_id');
            if ($id) {
                $model->load($id);
            }

            // Save Image
            if(isset($data['image']['delete'])){
                $path = $this->_objectManager->get(\Magento\Framework\Filesystem::class)->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('/').$data['image']['value'];
                unlink($path);
                $data['image'] = '';
            }
            elseif ($_FILES['image']['name']) {
                $uploader = $this->_objectManager->create(
                    \Magento\MediaStorage\Model\File\Uploader::class,
                    ['fileId' => 'image']
                );
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                $imageAdapter = $this->_objectManager->get(\Magento\Framework\Image\AdapterFactory::class)->create();
                $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                $mediaDirectory = $this->_objectManager->get(\Magento\Framework\Filesystem::class)
                    ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                // Save Image
                $result = $uploader->save($mediaDirectory->getAbsolutePath('banner'));

                $data['image'] = 'banner'.$result['file'];
            }else{
                $data['image'] = $data['image']['value'];
            }

            $model->setData($data);


            try {



                // Save Banner Info
                $model->save();


                $this->messageManager->addSuccess(__('Banner saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the block'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['banner_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }


}