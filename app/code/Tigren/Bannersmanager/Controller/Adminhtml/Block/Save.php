<?php

namespace Tigren\Bannersmanager\Controller\Adminhtml\Block;

use Magento\Backend\App\Action;
use Tigren\Bannersmanager\Model\Block;

class Save extends Action
{
    /**
     * @var \Tigren\Bannersmanager\Model\Block
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \Tigren\Bannersmanager\Model\Block $model
     */
    public function __construct(
        Action\Context $context,
        Block $model
    )
    {
        parent::__construct($context);
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_Bannersmanager::block_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Tigren\Bannersmanager\Model\Block $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            // set customer group id from Array to String
            $customerGroupId = implode("|", $model->getCustomerGroup());
            $model->setCustomerGroupId($customerGroupId);


            try {
                // Save Block Info
                $model->save();


                $this->messageManager->addSuccess(__('Block saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the block'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['block_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }


}