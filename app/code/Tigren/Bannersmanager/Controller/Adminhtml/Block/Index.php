<?php
namespace Tigren\Bannersmanager\Controller\Adminhtml\Block;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Tigren_Bannersmanager::block';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;


    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Tigren_Bannersmanager::block');
        $resultPage->addBreadcrumb(__('Blocks'), __('Blocks'));
        $resultPage->addBreadcrumb(__('Manage Blocks'), __('Manage Blocks'));
        $resultPage->getConfig()->getTitle()->prepend(__('Block'));

        return $resultPage;
    }
}