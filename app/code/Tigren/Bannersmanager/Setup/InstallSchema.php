<?php

namespace Tigren\Bannersmanager\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'tigren_bannersmanager_banner'
         */


        if (!$installer->tableExists('tigren_bannersmanager_banner')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('tigren_bannersmanager_banner')
            )
                ->addColumn(
                    'banner_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Banner ID'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    '64k',
                    ['nullable' => true],
                    'Description'
                )
                ->addColumn(
                    'banner_title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Banner Title'
                )
                ->addColumn(
                    'image',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Banner Image'
                )
                ->addColumn(
                    'banner_url',
                    Table::TYPE_TEXT,
                    255,
                    [],
                    'Banner URL'
                )
                ->addColumn(
                    'target',
                    Table::TYPE_INTEGER,
                    1,
                    [],
                    'Target'
                )
                ->addColumn(
                    'from',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Banner From Date'
                )
                ->addColumn(
                    'to',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Banner To Date'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_INTEGER,
                    1,
                    ['nullable' => false],
                    'Banner Status'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Sort Order'
                )
                ->addColumn(
                    'created',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )->addColumn(
                    'modified',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Modified At')
                ->setComment('Banner Table');
            $installer->getConnection()->createTable($table);

            /**
             * Add full text index to our table tigren_bannersmanager_banner
             */

            $tableName = $installer->getTable('tigren_bannersmanager_banner');
            $fullTextIntex = array('banner_title', 'banner_url', 'description'); // Column with fulltext index, you can put multiple fields

            $setup->getConnection()->addIndex(
                $tableName,
                $installer->getIdxName($tableName, $fullTextIntex, AdapterInterface::INDEX_TYPE_FULLTEXT),
                $fullTextIntex,
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        /**
         * Create table 'tigren_bannersmanager_block'
         */

        if (!$installer->tableExists('tigren_bannersmanager_block')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('tigren_bannersmanager_block')
            )
                ->addColumn(
                    'block_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Block ID'
                )
                ->addColumn(
                    'block_title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Block Title'
                )
                ->addColumn(
                    'customer_group_id',
                    Table::TYPE_TEXT,
                    '64k',
                    ['nullable' => false],
                    'Block Customer Group ID'
                )
                ->addColumn(
                    'position',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Block Position'
                )
                ->addColumn(
                    'category',
                    Table::TYPE_TEXT,
                    '64k',
                    ['nullable' => false],
                    'Block Categories'
                )
                ->addColumn(
                    'from',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Block From Date'
                )
                ->addColumn(
                    'to',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Block To Date'
                )
                ->addColumn(
                    'display_type',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Block Display Type'
                )
                ->addColumn(
                    'min_image',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Block Min Image'
                )
                ->addColumn(
                    'max_image',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Block Max Image'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_INTEGER,
                    1,
                    ['nullable' => false],
                    'Banner Status'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Sort Order'
                )
                ->addColumn(
                    'created',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Block Created At'
                )->addColumn(
                    'modified',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Block Modified At')
                ->setComment('Block Table');
            $installer->getConnection()->createTable($table);

            /**
             * Add full text index to our table tigren_bannersmanager_block
             */

            $tableName = $installer->getTable('tigren_bannersmanager_block');
            $fullTextIntex = array('block_title', 'position', 'display_type'); // Column with fulltext index, you can put multiple fields

            $setup->getConnection()->addIndex(
                $tableName,
                $installer->getIdxName($tableName, $fullTextIntex, AdapterInterface::INDEX_TYPE_FULLTEXT),
                $fullTextIntex,
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        /**
         * Create table 'tigren_bannersmanager_block_banner'
         */

        if (!$installer->tableExists('tigren_bannersmanager_block_banner')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('tigren_bannersmanager_block_banner')
            )
                ->addColumn(
                    'block_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => false,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Block ID'
                )
                ->addColumn(
                    'banner_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => false,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Banner ID'
                )
                ->addForeignKey(
                    $installer->getFkName('tigren_bannersmanager_block_banner', 'banner_id', 'tigren_bannersmanager_banner', 'banner_id'),
                    'banner_id',
                    $installer->getTable('tigren_bannersmanager_banner'), /* main table name */
                    'banner_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName('tigren_bannersmanager_block_banner', 'block_id', 'tigren_bannersmanager_block', 'block_id'),
                    'block_id',
                    $installer->getTable('tigren_bannersmanager_block'), /* main table name */
                    'block_id',
                    Table::ACTION_CASCADE
                )

                ->setComment('Block Banner Table');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}