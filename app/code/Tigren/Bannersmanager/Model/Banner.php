<?php
namespace Tigren\Bannersmanager\Model;

use \Magento\Framework\Model\AbstractModel;


class Banner extends AbstractModel
{

    const BANNER_ID = 'banner_id'; // We define the id fieldname

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'bannersmanager';

    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'banner';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = self::BANNER_ID;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Bannersmanager\Model\ResourceModel\Banner');
    }

    public function getEnableStatus() {
        return 1;
    }

    public function getDisableStatus() {
        return 0;
    }

    public function getAvailableStatuses(){
        return [$this->getDisableStatus() => __('Disabled'), $this->getEnableStatus() => __('Enabled')];
    }

    public function getBannerImages($banner_ids){
        foreach ($banner_ids as $banner_id){
            $banner = $this->load($banner_id);
            $bannerImage[] = $banner->getData('image');
        }
        return $bannerImage;
    }
}
