<?php
namespace Tigren\Bannersmanager\Model\ResourceModel\Banner;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName = \Tigren\Bannersmanager\Model\Banner::BANNER_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Bannersmanager\Model\Banner', 'Tigren\Bannersmanager\Model\ResourceModel\Banner');
    }

    public function getBannerIdsByBlockId($block_id){
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('tigren_bannersmanager_block_banner'),['banner_id'])
            ->where('block_id = :block_id');

        $banner_ids = $this->getConnection()->fetchCol($select, [':block_id' => $block_id]);

        return $banner_ids;
    }

    public function getBannersByBlockId($block_id){
        $banner_ids = $this->getBannerIdsByBlockId($block_id);
        $bannerCollection = $this
            ->addFieldToFilter('banner_id',array('in' => $banner_ids))
            ->load();
        return $bannerCollection;
    }

    public function getAvailBannersByBlockId($block_id){
        $banner_ids = $this->getBannerIdsByBlockId($block_id);
        $now = new \DateTime();
        $availBannerCollection = $this
            ->addFieldToFilter('banner_id',array('in' => $banner_ids))
            ->addFieldToFilter('from', ['lteq' => $now->format('Y-m-d H:i:s')])
            ->addFieldToFilter('to', ['gteq' => $now->format('Y-m-d H:i:s')])
            ->load();
        return $availBannerCollection;
    }
}