<?php
namespace Tigren\Bannersmanager\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
/**
 * Department post mysql resource
 */
class Banner extends AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init('tigren_bannersmanager_banner', 'banner_id');
    }

    /**
     * Method to run after load
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('tigren_bannersmanager_block_banner'),['block_id'])
            ->where('banner_id = :banner_id');

        $blocks = $this->getConnection()->fetchCol($select, [':banner_id' => $object->getId()]);

        if ($blocks) {
            $object->setData('blocks', $blocks);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Method to run after save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->getConnection()->delete(
            $this->getTable('tigren_bannersmanager_block_banner'),
            ['banner_id = ?' => $object->getData('banner_id')]
        );

        if ($this->hasSelectedBlocks($object->getData('selected_blocks'))) {
            foreach ((array)$object->getData('selected_blocks') as $block_id) {
                $storeArray = [
                    'banner_id' => $object->getId(),
                    'block_id' => $block_id
                ];
                $this->getConnection()->insert($this->getTable('tigren_bannersmanager_block_banner'), $storeArray);
            }

            return parent::_afterSave($object);
        }

        return parent::_afterSave($object);

    }

    protected function hasSelectedBlocks($arr)
    {
        if (is_array($arr)) return 1;
        else return 0;
    }

}