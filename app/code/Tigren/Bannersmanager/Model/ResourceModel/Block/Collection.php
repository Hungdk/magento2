<?php

namespace Tigren\Bannersmanager\Model\ResourceModel\Block;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Tigren\Bannersmanager\Model\ResourceModel\Block
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = \Tigren\Bannersmanager\Model\Block::BLOCK_ID;


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Bannersmanager\Model\Block', 'Tigren\Bannersmanager\Model\ResourceModel\Block');
    }

    /**
     * @return array
     */
    public function getEnableBlockIds()
    {
        $blockCollection = $this
            ->addFieldToSelect('block_id')
            ->addFieldToFilter('status', 1);
        foreach ($blockCollection as $block) {
            $block_ids[] = $block->getBlockId();
        }
        return $block_ids;
    }

}