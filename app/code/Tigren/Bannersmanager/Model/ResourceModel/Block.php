<?php

namespace Tigren\Bannersmanager\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Department post mysql resource
 */
class Block extends AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init('tigren_bannersmanager_block', 'block_id');
    }

    /**
     * Method to run after load
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable('tigren_bannersmanager_block_banner'),['banner_id'])
            ->where('block_id = :block_id');

        $banners = $this->getConnection()->fetchCol($select, [':block_id' => $object->getId()]);

        if ($banners) {
            $object->setData('banners', $banners);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Method to run after save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->getConnection()->delete(
            $this->getTable('tigren_bannersmanager_block_banner'),
            ['block_id = ?' => $object->getData('block_id')]
        );

        if ($this->hasSelectedBanners($object->getData('selected_banners'))) {
            foreach ((array)$object->getData('selected_banners') as $banner_id) {
                $storeArray = [
                    'block_id' => $object->getId(),
                    'banner_id' => $banner_id
                ];
                $this->getConnection()->insert($this->getTable('tigren_bannersmanager_block_banner'), $storeArray);
            }

            return parent::_afterSave($object);
        }

        return parent::_afterSave($object);

    }

    protected function hasSelectedBanners($arr)
    {
        if (is_array($arr)) return 1;
        else return 0;
    }

}