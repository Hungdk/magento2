<?php
namespace Tigren\Bannersmanager\Model\Source\Banner;

class Status implements \Magento\Framework\Data\OptionSourceInterface{

    /**
     * @var \Tigren\Bannersmanager\Model\Banner
     */
    protected $_banner;

    /**
     * Constructor
     *
     * @param \Tigren\Bannersmanager\Model\Banner $banner
     */
    public function __construct(\Tigren\Bannersmanager\Model\Banner $banner)
    {
        $this->_banner = $banner;

    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_banner->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/dachieu.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
////        $logger->info(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50), true)); // log trace
//        $logger->info(print_r('status',true)); // log $data
//        $logger->info(print_r($options,true)); // log $data
        return $options;
    }
}
