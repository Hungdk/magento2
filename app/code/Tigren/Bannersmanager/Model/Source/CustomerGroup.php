<?php
namespace Tigren\Bannersmanager\Model\Source;
use Magento\Customer\Model\Group;

class CustomerGroup implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Maxime\Jobs\Model\Department
     */
    protected $_customerGroup;

    /**
     * Constructor
     *
     * @param \Magento\Customer\Model\Group $customerGroup
     */
    public function __construct(Group $customerGroup)
    {
        $this->_customerGroup = $customerGroup;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $customerGroupCollection = $this->_customerGroup->getCollection()
            ->addFieldToSelect('customer_group_id')
            ->addFieldToSelect('customer_group_code');

        foreach ($customerGroupCollection as $customerGroup) {
            $options[] = [
                'label' => $customerGroup->getData('customer_group_code'),
                'value' => $customerGroup->getData('customer_group_id'),
            ];
        }
        return $options;
    }
}