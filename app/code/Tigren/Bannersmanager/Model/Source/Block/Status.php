<?php
namespace Tigren\Bannersmanager\Model\Source\Block;

class Status implements \Magento\Framework\Data\OptionSourceInterface{

    /**
     * @var \Tigren\Bannersmanager\Model\Block
     */
    protected $_block;

    /**
     * Constructor
     *
     * @param \Tigren\Bannersmanager\Model\Block $block
     */
    public function __construct(\Tigren\Bannersmanager\Model\Block $block)
    {
        $this->_block = $block;

    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_block->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/dachieu.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
////        $logger->info(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50), true)); // log trace
//        $logger->info(print_r('status',true)); // log $data
//        $logger->info(print_r($options,true)); // log $data
        return $options;
    }
}
