<?php
namespace Tigren\Bannersmanager\Model;

use \Magento\Framework\Model\AbstractModel;


class Block extends AbstractModel
{

    const BLOCK_ID = 'block_id'; // We define the id fieldname

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'bannersmanager';

    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'block';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = self::BLOCK_ID;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Bannersmanager\Model\ResourceModel\Block');
    }

    public function getEnableStatus() {
        return 1;
    }

    public function getDisableStatus() {
        return 0;
    }

    public function getAvailableStatuses(){
        return [$this->getDisableStatus() => __('Disabled'), $this->getEnableStatus() => __('Enabled')];
    }
}
