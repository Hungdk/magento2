<?php

namespace Maxime\Jobs\Block\Adminhtml\Job\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Maxime\Jobs\Model\Source\Job
     */
    protected $_deparment;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Maxime\Jobs\Model\Source\Department $department
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Maxime\Jobs\Model\Source\Department $department,
        array $data = []
    )
    {
        $this->_systemStore = $systemStore;
        $this->_deparment = $department;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('job_form');
        $this->setTitle(__('Job Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Maxime\Jobs\Model\Job $model */
        $model = $this->_coreRegistry->registry('jobs_job');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => [
                'id' => 'edit_form',
                'action' => $this->getData('action'),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
                ]
            ]
        );

        $form->setHtmlIdPrefix('job_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        }

        // Title Field
        $fieldset->addField(
            'title',
            'text',
            ['name' => 'title', 'label' => __('Job Title'), 'title' => __('Job Title'), 'required' => true]
        );


        // Type Field
        $fieldset->addField(
            'type',
            'text',
            ['name' => 'type', 'label' => __('Job Type'), 'title' => __('Job Type'), 'required' => true]
        );

        // Location Field
        $fieldset->addField(
            'location',
            'text',
            ['name' => 'location', 'label' => __('Job Location'), 'title' => __('Job Location'), 'required' => true]
        );

        // Date Field
        $fieldset->addField(
            'date',
            'date',
            [
                'name' => 'date',
                'label' => __('Job Date Begin'),
                'title' => __('Job Date Begin'),
                'date_format' => 'dd-MM-yyyy',
                'required' => false]
        );

        // Status Field
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Job Status'),
                'title' => __('Job Status'),
                'values' => [
                    ['label' => __('Disable'), 'value' => 0],
                    ['label' => __('Enable'), 'value' => 1]
                ],
                'required' => true
            ]
        );

        // Description Field
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Job Description'),
                'title' => __('Job Description'),
                'required' => true,
            ]
        );

        // Department Id Field
        $departments = $this->_deparment->toOptionArray();
        $fieldset->addField(
            'department_id',
            'select',
            [
                'name' => 'department_id',
                'label' => __('Department'),
                'title' => __('Department'),
                'required' => true,
                'values' => $departments
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        // Image Field
        $fieldset->addField(
            'image',
            'image',
            [
                'title' => __('Image'),
                'label' => __('Image'),
                'name' => 'image',
            ]
        );

        return parent::_prepareForm();
    }
}