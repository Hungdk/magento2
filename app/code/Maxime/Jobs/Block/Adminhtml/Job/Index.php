<?php
namespace Maxime\Jobs\Block\Adminhtml\Job;

use Magento\Backend\Block\Widget\Grid\Container;
class Index extends Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_job';
        $this->_blockGroup = 'Maxium Job';
        $this->_headerText = __('Job');
        $this->_addButtonLabel = __('Add New Job');
        parent::_construct();
    }
}