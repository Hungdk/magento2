<?php
namespace Maxime\Jobs\Model\Source\Job;

class Status implements \Magento\Framework\Data\OptionSourceInterface{

    /**
     * @var \Maxime\Jobs\Model\Job
     */
    protected $_job;

    /**
     * Constructor
     *
     * @param \Maxime\Jobs\Model\Job $job
     */
    public function __construct(\Maxime\Jobs\Model\Job $job)
    {
        $this->_job = $job;

    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_job->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/dachieu.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
////        $logger->info(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50), true)); // log trace
//        $logger->info(print_r('status',true)); // log $data
//        $logger->info(print_r($options,true)); // log $data
        return $options;
    }
}
