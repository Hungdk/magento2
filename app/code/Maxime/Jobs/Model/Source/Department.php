<?php
namespace Maxime\Jobs\Model\Source;

class Department implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Maxime\Jobs\Model\Department
     */
    protected $_department;

    /**
     * Constructor
     *
     * @param \Maxime\Jobs\Model\Department $department
     */
    public function __construct(\Maxime\Jobs\Model\Department $department)
    {
        $this->_department = $department;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $departmentCollection = $this->_department->getCollection()
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('name');
        foreach ($departmentCollection as $department) {
            $options[] = [
                'label' => $department->getName(),
                'value' => $department->getId(),
            ];
        }
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/dachieu.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
////        $logger->info(print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50), true)); // log trace
//        $logger->info(print_r('depart',true)); // log $data
//
//        $logger->info(print_r($options,true)); // log $data
        return $options;
    }
}